//
//  HuePulser.swift
//  LinkSmartForDummies
//
//  Created by Daniel Eriksson on 13/03/15.
//  Copyright (c) 2015 Daniel Eriksson. All rights reserved.
//

import UIKit

class HuePulser: NSObject {
    
    private var pulseNow = false
    
    func stopPulse()
    {
        pulseNow = false
    }
    func togglePulse()
    {
        if pulseNow
        {
            stopPulse()
        }
        else
        {
            startPulse()
        }
    }
    
    func startPulse()
    {
        if pulseNow
        {
            return
        }
        pulseNow = true;
        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        dispatch_async(backgroundQueue, {
            while(self.pulseNow)
            {
                self.pulseSyncWithName("HueSpotLeft")
                self.pulseSyncWithName("HueSpotCenter")
                self.pulseSyncWithName("HueSpotRight")
                NSThread.sleepForTimeInterval(1)
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                })
            }
            self.turnOffSyncWithName("HueSpotLeft")
            self.turnOffSyncWithName("HueSpotCenter")
            self.turnOffSyncWithName("HueSpotRight")
        })
    }
    
    private func pulseSyncWithName(name : String)
    {
        let r1 = String(arc4random_uniform(256))
        let r2 = String(arc4random_uniform(256))
        let r3 = String(arc4random_uniform(256))
        
        let url = NSURL(string: "http://hydra.cnet.se:44441/IoTresources/" + name + "/services/multicolordimmer/actions/SetRGBColor?red=" + r1 + "&green=" + r2 + "&blue=" + r3 )
        let request = NSMutableURLRequest(URL: url!)
        
        var response: NSURLResponse?
        var error: NSError?
        let urlData: NSData?
        do {
            urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
        } catch let error1 as NSError {
            error = error1
            urlData = nil
        }

    }
    
    private func turnOffSyncWithName(name : String)
    {
        let url = NSURL(string: "http://hydra.cnet.se:44441/IoTresources/" + name + "/services/multicolordimmer/actions/TurnOff" )
        let request = NSMutableURLRequest(URL: url!)
        var response: NSURLResponse?
        var error: NSError?
        let urlData: NSData?
        do {
            urlData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
        } catch let error1 as NSError {
            error = error1
            urlData = nil
        }
    }
}
