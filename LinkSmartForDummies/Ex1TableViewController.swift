//
//  Ex1TableViewController.swift
//  LinkSmartForDummies
//
//  Created by Daniel Eriksson on 13/03/15.
//  Copyright (c) 2015 Daniel Eriksson. All rights reserved.
//

import UIKit

class Ex1TableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        switch (indexPath.row){
        case 0:
            self.turnOnDiscoBall()
        case 1:
            self.turnOffDiscoBall()
        case 2:
            self.showDiscoBallStatus()
        case 3:
            self.toggleDiscoBallStatus()
        default:
            return
        }
    }
    
    func deselectCurrentRow()
    {
        if self.tableView.indexPathForSelectedRow == nil
        {
            return
        }

        dispatch_async(dispatch_get_main_queue(),{
            self.tableView.deselectRowAtIndexPath(self.tableView.indexPathForSelectedRow!, animated: true)
        });
    }
    
    func turnOnDiscoBall()
    {
        let url = NSURL(string: "http://hydra.cnet.se:44441/IoTresources/DiscoBall/services/switch/actions/TurnOn")
        let request = NSMutableURLRequest(URL: url!)
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            self.deselectCurrentRow()
        })
        
    }
    func turnOffDiscoBall()
    {
        let url = NSURL(string: "http://hydra.cnet.se:44441/IoTresources/DiscoBall/services/switch/actions/TurnOff")
        let request = NSMutableURLRequest(URL: url!)
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            self.deselectCurrentRow()
        })
        
    }
    
    func showDiscoBallStatus()
    {
        let url = NSURL(string: "http://hydra.cnet.se:44441/IoTresources/DiscoBall/services/switch/actions/GetSwitchStatus")
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            self.deselectCurrentRow()
            
            if data != nil
            {
                do {
                    let jsonDict = (try NSJSONSerialization.JSONObjectWithData(data ?? NSData(), options: NSJSONReadingOptions.MutableContainers) as? NSDictionary) ?? NSDictionary()
                    if let res = jsonDict["result"] as? NSDictionary{
                        let value = (res["value"] as? String) ?? ""
                        let alert = UIAlertController(title: "Response", message: value, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                } catch {
                    NSLog("FATAL ERROR")
                    fatalError()
                }
            }
        })
    }
    
    
    func toggleDiscoBallStatus()
    {
        let url = NSURL(string: "http://hydra.cnet.se:44441/IoTresources/DiscoBall/services/switch/actions/GetSwitchStatus")
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            
            if data != nil
            {
                do {
                    let jsonDict = (try NSJSONSerialization.JSONObjectWithData(data ?? NSData(), options: NSJSONReadingOptions.MutableContainers) as? NSDictionary) ?? NSDictionary()
                    if let res = jsonDict["result"] as? NSDictionary {
                        let value = (res["value"] as? String) ?? ""
                        
                        var toTurn = "Off"
                        if value.lowercaseString=="off"
                        {
                            toTurn = "On"
                        }
                        let request2 = NSMutableURLRequest(URL: NSURL(string: "http://hydra.cnet.se:44441/IoTresources/DiscoBall/services/switch/actions/Turn" + toTurn)!)
                        NSURLConnection.sendAsynchronousRequest(request2, queue: NSOperationQueue(), completionHandler:{ (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
                            self.deselectCurrentRow()
                        })
                        
                    }
                } catch {
                    NSLog("FATAL ERROR")
                    fatalError()
                }
                
            }
        })
    }
  
    

}
