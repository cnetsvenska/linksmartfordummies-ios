//
//  Ex2TableViewController.swift
//  LinkSmartForDummies
//
//  Created by Daniel Eriksson on 13/03/15.
//  Copyright (c) 2015 Daniel Eriksson. All rights reserved.
//

import UIKit

class Ex2TableViewController: UITableViewController {

    var huePulser = HuePulser()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        switch (indexPath.row){
        case 0:
            self.setHueCenterBlue()
        case 1:
            self.deselectCurrentRow()
            huePulser.togglePulse()
        case 2:
            huePulser.startPulse()
            self.turnOnDiscoBall()
        default:
            return
        }
    }
    
    func deselectCurrentRow()
    {
        if self.tableView.indexPathForSelectedRow == nil
        {
            return
        }
        dispatch_async(dispatch_get_main_queue(),{
            self.tableView.deselectRowAtIndexPath(self.tableView.indexPathForSelectedRow!, animated: true)
        });
    }
    
    func setHueCenterBlue()
    {
        let url = NSURL(string: "http://hydra.cnet.se:44441/IoTresources/HueSpotCenter/services/multicolordimmer/actions/SetRGBColor?blue=255")
        let request = NSMutableURLRequest(URL: url!)
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            self.deselectCurrentRow()
        })
        
    }
    
    func turnOnDiscoBall()
    {
        let url = NSURL(string: "http://hydra.cnet.se:44441/IoTresources/DiscoBall/services/switch/actions/TurnOn")
        let request = NSMutableURLRequest(URL: url!)
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            self.deselectCurrentRow()
        })
        
    }
    /*func turnOffDiscoBall()
    {
        let url = NSURL(string: "http://hydra.cnet.se:44441/IoTresources/DiscoBall/services/switch/actions/TurnOff")
        let request = NSMutableURLRequest(URL: url!)
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            self.deselectCurrentRow()
        })
        
    }*/
    
}
